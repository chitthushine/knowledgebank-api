from flask import Flask, render_template, json, request
from flaskext.mysql import MySQL

#Dataframe manipulation library
import pandas as pd
#Math functions, we'll only need the sqrt function so let's import only that
from math import sqrt
import numpy as np

mysql = MySQL()
app = Flask(__name__)

def getPearsonCorrelationDict(userSubsetGroup, inputQuestions):
    #Store the Pearson Correlation in a dictionary, where the key is the user Id and the value is the coefficient
    pearsonCorrelationDict = {}

    #For every user group in our subset
    for name, group in userSubsetGroup:
        #Let's start by sorting the input and current user group so the values aren't mixed up later on
        group = group.sort_values(by='questionId')
        inputQuestions = inputQuestions.sort_values(by='questionId')
        #Get the N for the formula
        nRatings = len(group)
        #Get the review scores for the questions that they both have in common
        temp_df = inputQuestions[inputQuestions['questionId'].isin(group['questionId'].tolist())]
        #And then store them in a temporary buffer variable in a list format to facilitate future calculations
        tempRatingList = temp_df['rating'].tolist()
        #Let's also put the current user group reviews in a list format
        tempGroupList = group['rating'].tolist()
        #Now let's calculate the pearson correlation between two users, so called, x and y
        Sxx = sum([i**2 for i in tempRatingList]) - pow(sum(tempRatingList),2)/float(nRatings)
        Syy = sum([i**2 for i in tempGroupList]) - pow(sum(tempGroupList),2)/float(nRatings)
        Sxy = sum( i*j for i, j in zip(tempRatingList, tempGroupList)) - sum(tempRatingList)*sum(tempGroupList)/float(nRatings)
        
        #If the denominator is different than zero, then divide, else, 0 correlation.
        if Sxx != 0 and Syy != 0:
            pearsonCorrelationDict[name] = Sxy/sqrt(Sxx*Syy)
        else:
            pearsonCorrelationDict[name] = 0
    return pearsonCorrelationDict

@app.route('/getRecommendedQuestion',methods=['POST','GET'])
def getRecommendedQuestion():
    #Storing the movie information into a pandas dataframe
    questions_df = pd.read_csv('./data/tbl_question.csv')
    ratings_df = pd.read_csv('./data/tbl_rating.csv')
    userInput = [
            {'question':'Breakfast Club, The', 'rating':5},
            {'question':'Toy Story', 'rating':3.5},
            {'question':'Jumanji', 'rating':2},
            {'question':"Pulp Fiction", 'rating':5},
            {'question':'Akira', 'rating':4.5}
         ] 
    inputQuestions = pd.DataFrame(userInput)
    #Add questionsId
    inputId = questions_df[questions_df['question'].isin(inputQuestions['question'].tolist())]
    #Then merging it so we can get the questionId. It's implicitly merging it by title.
    inputQuestions = pd.merge(inputId, inputQuestions)
    #Dropping information we won't use from the input dataframe
    inputQuestions = inputQuestions.drop('tsCreate', 1)
    #Filtering out users that have answered questions that the input has answered and storing it
    userSubset = ratings_df[ratings_df['questionId'].isin(inputQuestions['questionId'].tolist())]
    #Groupby creates several sub dataframes where they all have the same value in the column specified as the parameter
    userSubsetGroup = userSubset.groupby(['userId'])
    #Sorting it so users with questions most in common with the input will have priority
    userSubsetGroup = sorted(userSubsetGroup,  key=lambda x: len(x[1]), reverse=True)
    userSubsetGroup = userSubsetGroup[0:100]

    #Store the Pearson Correlation in a dictionary, where the key is the user Id and the value is the coefficient
    pearsonCorrelationDict = {}
    pearsonCorrelationDict = getPearsonCorrelationDict(userSubsetGroup, inputQuestions)

    pearsonDF = pd.DataFrame.from_dict(pearsonCorrelationDict, orient='index')
    pearsonDF.columns = ['similarityIndex']
    pearsonDF['userId'] = pearsonDF.index
    pearsonDF.index = range(len(pearsonDF))
    topUsers=pearsonDF.sort_values(by='similarityIndex', ascending=False)[0:50]
    topUsersRating=topUsers.merge(ratings_df, left_on='userId', right_on='userId', how='inner')
    #Multiplies the similarity by the user's ratings
    topUsersRating['weightedRating'] = topUsersRating['similarityIndex']*topUsersRating['rating']

    #Applies a sum to the topUsers after grouping it up by userId
    tempTopUsersRating = topUsersRating.groupby('questionId').sum()[['similarityIndex','weightedRating']]
    tempTopUsersRating.columns = ['sum_similarityIndex','sum_weightedRating']

    #Creates an empty dataframe
    recommendation_df = pd.DataFrame()
    #Now we take the weighted average
    recommendation_df['weighted average recommendation score'] = tempTopUsersRating['sum_weightedRating']/tempTopUsersRating['sum_similarityIndex']
    recommendation_df['questionId'] = tempTopUsersRating.index
    recommendation_df = recommendation_df.sort_values(by='weighted average recommendation score', ascending=False)
    top10recommended_df = questions_df.loc[questions_df['questionId'].isin(recommendation_df.head(10)['questionId'].tolist())]

    # for index, row in top10recommended_df.iterrows():
    #     print(row['question'])
    d = [ 
        dict([
            (colname, row[i]) 
            for i,colname in enumerate(top10recommended_df.columns)
        ])
        for row in top10recommended_df.values
    ]


    return json.dumps(d)

if __name__ == "__main__":
    app.run(port=5000)
